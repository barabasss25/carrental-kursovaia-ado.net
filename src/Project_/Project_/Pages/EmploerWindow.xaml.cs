﻿using Business_Logic.DTO;
using Business_Logic.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project_.Pages
{
    /// <summary>
    /// Interaction logic for EmploerWindow.xaml
    /// </summary>
    public partial class EmploerWindow : Window
    {
        List<bool> tmp =new List<bool>();
        private readonly EmploerServiceFacad emploerService;
        public EmploerWindow(EmploerServiceFacad emploerService)
        {
            for (int i = 0; i < 5; i++) tmp.Add(false);
            this.emploerService=emploerService;
            InitializeComponent();
        }

        private async void Button_Click_Transport(object sender, RoutedEventArgs e)
        {
            tmp[0] = true;
            DataGridEmploer.IsReadOnly = false;
            DataGridEmploer.ItemsSource = await emploerService.TransportService.GetAllAsync();
        }

        private async void Button_Click_User(object sender, RoutedEventArgs e)
        {
            tmp[1] = true;
            DataGridEmploer.ItemsSource = await emploerService.UserService.GetAllAsync();
            DataGridEmploer.IsReadOnly = false;
        }

        private async void Button_Click_TravelHistory(object sender, RoutedEventArgs e)
        {
            tmp[2] = true;
            DataGridEmploer.IsReadOnly = true;
            DataGridEmploer.ItemsSource = await emploerService.TravelHistoryService.GetAllAsync();
        }


        private void Button_Click_Exit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private async void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            foreach(var item in DataGridEmploer.ItemsSource)
            {
                if (tmp[0] == true) await emploerService.TransportService.Update((TransportDTO)item);
                else if (tmp[1] == true) await emploerService.UserService.Update((UserDTO)item);
                else if (tmp[2] == true) await emploerService.TravelHistoryService.Update((TravelHistoryDTO)item);
                else if (tmp[3] == true) await emploerService.MoneyTransferExpensesService.Update((MoneyTransferExpensesDTO)item);
                else if (tmp[4] == true) await emploerService.MoneyTransferSalaryService.Update((MoneyTransferSalaryDTO)item);
            }
        }

        private async void Button_Click_Money_Transfer_Expenses(object sender, RoutedEventArgs e)
        {
            DataGridEmploer.ItemsSource = await emploerService.MoneyTransferExpensesService.GetAllAsync();
            tmp[3] = true;
            DataGridEmploer.IsReadOnly = true;
        }

        private async void Button_Click_Money_Transfer_Salary(object sender, RoutedEventArgs e)
        {
            DataGridEmploer.ItemsSource = await emploerService.MoneyTransferSalaryService.GetAllAsync();
            tmp[4] = true;
            DataGridEmploer.IsReadOnly = true;
        }
    }
}
