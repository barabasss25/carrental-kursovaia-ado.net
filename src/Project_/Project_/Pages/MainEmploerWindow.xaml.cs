﻿using Business_Logic.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project_.Pages
{
    /// <summary>
    /// Interaction logic for MainEmploerWindow.xaml
    /// </summary>
    public partial class MainEmploerWindow : Window
    {
        TransportService transportService;
        UserService userService;
        public MainEmploerWindow(TransportService transportService, UserService userService)
        {
            this.transportService = transportService;
            this.userService = userService;
            InitializeComponent();
        }

        private async void Button_Click_Transport(object sender, RoutedEventArgs e)
        {
            DataGridEmploer.ItemsSource = await transportService.GetAllAsync();
        }

        private async void Button_Click_User(object sender, RoutedEventArgs e)
        {
            DataGridEmploer.ItemsSource = await userService.GetAllAsync();
        }

        private void Button_Click_TravelHistory(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_Emploer(object sender, RoutedEventArgs e)
        {
        }

        private void Button_Click_Battery(object sender, RoutedEventArgs e)
        {
        }

        private void Button_Click_Exit(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.Close();
        }
    }
}
