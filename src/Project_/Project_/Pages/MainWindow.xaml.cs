﻿using Business_Logic.DTO;
using Business_Logic.Service;
using Microsoft.Extensions.DependencyInjection;
using Project_.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window 
    {
        private readonly LoginAndPasswordService loginAndPasswordService;


        public MainWindow(LoginAndPasswordService loginAndPasswordService)
        {
            this.loginAndPasswordService = loginAndPasswordService;

            InitializeComponent();


        }
        private void Button_Click_Exit_Authorization(object sender, RoutedEventArgs e)
        {
            this.Close();

        }

        private async void Button_Click_Next_Authorization(object sender, RoutedEventArgs e)
        {

            if (Login.Text != "" && Password.Text != "")
            {
               foreach (var item in await loginAndPasswordService.GetAllAsync())
                {
                    if (item.Login == Login.Text && item.Password == Password.Text)
                    {
                        var window = App.ServiceProvider.GetRequiredService<EmploerWindow>();
                        var window2 = App.ServiceProvider.GetRequiredService<MainEmploerWindow>();
                        window.ShowDialog();
                    }
                }
               
            }
        }
    }
}
        
   
        
    





           
            
        



