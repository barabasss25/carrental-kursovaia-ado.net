﻿using AutoMapper;
using Business_Logic.Config;
using Business_Logic.Infrastructure.AutoMaper;
using Business_Logic.Service;
using Data_Access.Context;
using Data_Access.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Project_.Pages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Project_
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static IServiceProvider ServiceProvider { get; private set; }
        public IConfiguration Configuration { get; private set; }
        private void ConfigurationService(IServiceCollection services)
        {

            ConfigurationManagerBLL.Configuration(services, Configuration.GetConnectionString("SqlConnection"));


            services.AddTransient(typeof(MainWindow));
            services.AddTransient(typeof(MainEmploerWindow));
            services.AddTransient(typeof(EmploerWindow));
            services.AddTransient(typeof(UnitOfWork));
            services.AddTransient(typeof(EmploerService));
            services.AddTransient(typeof(UserService));
            services.AddTransient(typeof(BatteryService));
            services.AddTransient(typeof(FineService));
            services.AddTransient(typeof(LoginAndPasswordService));
            services.AddTransient(typeof(MoneyTransferExpensesService));
            services.AddTransient(typeof(MoneyTransferSalaryService));
            services.AddTransient(typeof(RequestService));
            services.AddTransient(typeof(TravelHistoryService));
            services.AddTransient(typeof(TransportService));
            services.AddTransient(typeof(EmploerServiceFacad));
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MapperConfig()); });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", false, reloadOnChange: true);
            Configuration = builder.Build();

            var serviceCollection = new ServiceCollection();
            ConfigurationService(serviceCollection);
            ServiceProvider = serviceCollection.BuildServiceProvider();

            ServiceProvider.GetRequiredService<MainWindow>().Show();




        }
    }
}

