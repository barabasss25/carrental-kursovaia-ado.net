﻿using Data_Access.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access.UnitOfWork
{
    public interface IUnitOfWork
    {
          UserRepository UserRepository { get; } 
          MoneyTransferExpensesRepository MoneyTransferExpenses_Repository { get; }
          MoneyTransferSalaryRepository MoneyTransferSalary_Repository { get; }
          TransportRepository Transport_Repository { get; }
          EmploerRepository Emploer_Repository { get; }
          LoginAndPasswordRepository LoginAndPassword_Repository { get; }
          BatteryRepository Battery_Repository { get; }
          FineRepository Fine_Repository { get; }
          RequestRepository Request_Repository { get; }
          TravelHistoryRepository TravelHistory_Repository { get; }


        Task SaveChangesAsync();
    }
}
