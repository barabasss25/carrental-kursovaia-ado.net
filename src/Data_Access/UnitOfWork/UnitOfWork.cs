﻿using Data_Access.Context;
using Data_Access.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {

        public UnitOfWork(UserRepository userRepository,
        MoneyTransferExpensesRepository moneyTransferExpenses_Repository,
        MoneyTransferSalaryRepository moneyTransferSalary_Repository,
        TransportRepository transport_Repository,
        EmploerRepository emploer_Repository,
        LoginAndPasswordRepository loginAndPassword_Repository,
        BatteryRepository battery_Repository,
        FineRepository fine_Repository,
        RequestRepository request_Repository,
        TravelHistoryRepository travelHistory_Repository,
        ContextDb contextDb)
        {
            UserRepository = userRepository;
            MoneyTransferExpenses_Repository = moneyTransferExpenses_Repository;
            MoneyTransferSalary_Repository = moneyTransferSalary_Repository;
            Transport_Repository = transport_Repository;
            Emploer_Repository = emploer_Repository;
            LoginAndPassword_Repository = loginAndPassword_Repository;
            Battery_Repository = battery_Repository;
            Fine_Repository = fine_Repository;
            Request_Repository = request_Repository;
            TravelHistory_Repository = travelHistory_Repository;
            Context_db = contextDb;
        }


        private ContextDb Context_db { get; }

        public void Dispose()
        {
            Context_db?.Dispose();
        }
        public Task SaveChangesAsync()
        {
            return Context_db.SaveChangesAsync();
        }


        public  UserRepository UserRepository { get; }
        public MoneyTransferExpensesRepository MoneyTransferExpenses_Repository { get; }
        public MoneyTransferSalaryRepository MoneyTransferSalary_Repository { get; }
        public TransportRepository Transport_Repository { get; }
        public EmploerRepository Emploer_Repository { get; }
        public LoginAndPasswordRepository LoginAndPassword_Repository { get; }
        public FineRepository Fine_Repository { get; }
        public RequestRepository Request_Repository { get; }
        public TravelHistoryRepository TravelHistory_Repository { get; }
        public BatteryRepository Battery_Repository { get; }
    }
}
