﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Access.Models
{
    public class User
    {
        public int Id { get; set; }
        public int? LoginAndPasswordId { get; set; }
        public LoginAndPassword LoginAndPassword { get; set; }
        public string Name { get; set; }
        public double Money { get; set; } 
        public string Number { get; set; } 
        public string Telegram { get; set; } 
        public double Bonuses { get; set; } 
        public string DateOfBirth { get; set; }
        public int TransportId { get; set;  }
        public Transport Transport { get; set; } 
        public List<TravelHistory> TravelHistories { get; set; }
        public List<MoneyTransferExpenses> MoneyTransferExpenses { get; set; }
    }
}
