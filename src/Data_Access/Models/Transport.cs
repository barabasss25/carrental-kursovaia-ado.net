﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Access.Models
{
    public class Transport
    {
        public int Id { get; set; }
        public User User { get; set; }
        public string TypeTransport { get; set; }
        public float CostPerHouse { get; set; }
        public string GeoLocation { get; set; }
        public int BatteryId { get; set; } 
        public Battery Battery { get; set; }
        public List<TravelHistory> TravelHistories { get; set; }
        public List<MoneyTransferExpenses> MoneyTransferExpenses { get; set; }
    }
}
