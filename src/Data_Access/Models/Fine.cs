﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Access.Models
{
    public class Fine
    {
        public int Id { get; set; }
        public Emploer Emploer { get; set; }
        public string Description { get; set; }
        public float Amount { get; set; }
        public MoneyTransferSalary MoneyTransferSalary { get; set; }
    }
}
