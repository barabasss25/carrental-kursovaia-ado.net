﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Access.Models
{
    public class TravelHistory
    {
        public int Id { get; set; }
        public User User { get; set; }
        public Transport Transport { get; set; }
        public float Cost { get; set; }
        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; } = DateTime.Now;
        }
}
