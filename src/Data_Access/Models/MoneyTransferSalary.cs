﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Access.Models
{
    public class MoneyTransferSalary
    {
        public int Id { get; set; }
        public Emploer Emploer { get; set; }
      //  public DateTime StartWorkDay { get; set; } = DateTime.Now;
        public int HoursWorked { get; set; }
        public List<Fine> Fines { get; set; }

    }
}
