﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Access.Models
{
    public class MoneyTransferExpenses
    {
        public int Id { get; set; }
        public User User { get; set; }
        public Emploer Emploer { get; set; }
        public string Description { get; set; }
        public double Money { get; set; } 
        public Transport Transport { get; set; }
    }
}
