﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Access.Models
{
    public class Emploer
    {
        public Emploer()
        {
            moneyTransferExpenses = new List<MoneyTransferExpenses>();
            moneyTransferSalaries = new List<MoneyTransferSalary>();
            TimeStartWorked = DateTime.Now;
        }
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Position { get; set; }
        public int LoginAndPasswordId { get; set; } 
        public LoginAndPassword LoginAndPassword { get; set; }
        public bool Online { get; set; }
        public DateTime TimeStartWorked { get; set; }
        public List<MoneyTransferExpenses> moneyTransferExpenses { get; set; }
        public List<MoneyTransferSalary> moneyTransferSalaries { get; set; }
    }
}
