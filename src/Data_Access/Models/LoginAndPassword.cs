﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Access.Models
{
    public class LoginAndPassword
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public DateTime DateRegistration { get; set; } = DateTime.Now;
        public User User { get; set; }
        public Emploer Emploer { get; set; }
    }
}
