﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Access.Models
{
    public class Battery
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Recharge { get; set; }
        public bool IfBroken { get; set; }
        public Transport Transport { get; set; }
    }
}
