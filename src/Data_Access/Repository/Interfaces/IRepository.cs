﻿using Data_Access.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access.Repository
{
    public interface IRepository<T>
    {
        public interface IRepository<TEntity>
        {
            Task<IEnumerable<TEntity>> GetAllAsync();
            Task<IReadOnlyCollection<TEntity>> FindByConditionAsync(Expression<Func<TEntity, bool>> predicat);
            Task<OperationDetail> CreateAsync(TEntity entity);
        }
    }
}
