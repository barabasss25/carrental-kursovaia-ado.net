﻿using Data_Access.Context;
using Data_Access.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access.Repository.Interfaces
{
        public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
        {
            protected BaseRepository(ContextDb context)
            {
                context_db = context;
            }
            private DbSet<TEntity> _entities;
            private ContextDb context_db;
            protected DbSet<TEntity> Entities => this._entities ??= context_db.Set<TEntity>();
            public virtual async Task<IEnumerable<TEntity>> GetAllAsync()
            {
                return await this.Entities.ToListAsync().ConfigureAwait(false);
            }
            public virtual async Task<IReadOnlyCollection<TEntity>> FindByConditionAsync(Expression<Func<TEntity, bool>> predicat)
            {
                return await this.Entities.Where(predicat).ToListAsync().ConfigureAwait(false);
            }
            public async Task<OperationDetail> CreateAsync(TEntity entity)
            {
                try
                {
                    await Entities.AddAsync(entity).ConfigureAwait(false);
                    return new OperationDetail { Message = "Created" };
                }
                catch (Exception e)
                {
                    Log.Error(e, "Create Fatal Error");
                    return new OperationDetail { IsError = true, Message = "Create Fatal Error" };
                }
            }
        }
    }
