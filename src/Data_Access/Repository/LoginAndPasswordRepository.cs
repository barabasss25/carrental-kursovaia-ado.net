﻿using Data_Access.Context;
using Data_Access.Models;
using Data_Access.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access.Repository
{
        public class LoginAndPasswordRepository : BaseRepository<LoginAndPassword>
        {
            public LoginAndPasswordRepository(ContextDb context) : base(context)
            {
            }
            public override async Task<IEnumerable<LoginAndPassword>> GetAllAsync()
            {
                return await this.Entities.ToListAsync().ConfigureAwait(false);
            }

            public override async Task<IReadOnlyCollection<LoginAndPassword>> FindByConditionAsync(Expression<Func<LoginAndPassword, bool>> predicat)
            {
                return await this.Entities.Where(predicat).ToListAsync().ConfigureAwait(false);
            }
        }
    }
