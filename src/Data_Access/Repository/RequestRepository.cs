﻿using Data_Access.Context;
using Data_Access.Models;
using Data_Access.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access.Repository
{
        public class RequestRepository : BaseRepository<Request>
        {
            public RequestRepository(ContextDb context) : base(context)
            {
            }
            public override async Task<IEnumerable<Request>> GetAllAsync()
            {
                return await this.Entities.ToListAsync().ConfigureAwait(false);
            }

            public override async Task<IReadOnlyCollection<Request>> FindByConditionAsync(Expression<Func<Request, bool>> predicat)
            {
                return await this.Entities.Where(predicat).ToListAsync().ConfigureAwait(false);
            }
        }
    }
