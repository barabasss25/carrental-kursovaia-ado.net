﻿using Data_Access.Context;
using Data_Access.Models;
using Data_Access.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access.Repository
{

        public class EmploerRepository : BaseRepository<Emploer>
        {
            public EmploerRepository(ContextDb context) : base(context)
            {
            }
            public override async Task<IEnumerable<Emploer>> GetAllAsync()
            {
            return await this.Entities
                .Include(x => x.LoginAndPassword).ThenInclude(x => x.User).ThenInclude(x=>x.MoneyTransferExpenses)
                .Include(x => x.LoginAndPassword).ThenInclude(x=>x.User).ThenInclude(x=>x.Transport).ThenInclude(x=>x.Battery)
                .Include(x=>x.LoginAndPassword).ThenInclude(x=>x.User).ThenInclude(x=>x.TravelHistories)
                .Include(x => x.moneyTransferExpenses).Include(x => x.moneyTransferSalaries).ToListAsync().ConfigureAwait(false);
            }

            public override async Task<IReadOnlyCollection<Emploer>>FindByConditionAsync(Expression<Func<Emploer, bool>> predicat)
            {
                return await this.Entities.Where(predicat).Include(x=>x.LoginAndPassword).Include(x=>x.moneyTransferExpenses).Include(x=>x.moneyTransferSalaries).Include(x => x.TimeStartWorked).ToListAsync().ConfigureAwait(false);
            }
        }
}
