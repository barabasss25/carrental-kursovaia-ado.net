﻿using Data_Access.Context;
using Data_Access.Models;
using Data_Access.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access.Repository
{
        public class MoneyTransferExpensesRepository : BaseRepository<MoneyTransferExpenses>
        {
            public MoneyTransferExpensesRepository(ContextDb context) : base(context)
            {
            }
            public override async Task<IEnumerable<MoneyTransferExpenses>> GetAllAsync()
            {
                return await this.Entities.ToListAsync().ConfigureAwait(false);
            }

            public override async Task<IReadOnlyCollection<MoneyTransferExpenses>> FindByConditionAsync(Expression<Func<MoneyTransferExpenses, bool>> predicat)
            {
                return await this.Entities.Where(predicat).ToListAsync().ConfigureAwait(false);
            }
        }
    }
