﻿using Data_Access.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data_Access.Context
{
    public class ContextDb:DbContext
    {

        public ContextDb(DbContextOptions<ContextDb> options) : base(options)
        {
            Database.EnsureCreated();
        }


        public DbSet<Battery> Batteries { get; set; }
        public DbSet<Emploer> Emploers { get; set; }
        public DbSet<Fine> Fines { get; set; }
        public DbSet<LoginAndPassword> LoginAndPasswords { get; set; }
        public DbSet<MoneyTransferExpenses> MoneyTransferExpenses { get; set; }
        public DbSet<MoneyTransferSalary> MoneyTransferSalaries { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Transport> Transports { get; set; }
        public DbSet<TravelHistory> TravelHistories { get; set; }
        public DbSet<User> Users { get; set; }



    }
}
