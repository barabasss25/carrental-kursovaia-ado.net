﻿using AutoMapper;
using Business_Logic.DTO;
using Data_Access.Infrastructure;
using Data_Access.Models;
using Data_Access.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic.Service
{
    public class MoneyTransferSalaryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public MoneyTransferSalaryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public MoneyTransferSalaryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task CreateAsync(MoneyTransferSalaryDTO entity)
        {
            await _unitOfWork.MoneyTransferSalary_Repository.CreateAsync((MoneyTransferSalary)_mapper.Map(entity, typeof(MoneyTransferSalaryDTO), typeof(MoneyTransferSalary)));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<MoneyTransferSalaryDTO>> FindByConditionAsync(int id)
        {
            // return _mapper.Map(await _unitOfWork.Emploer_Repository.FindByConditionAsync(predicat),typeof(IReadOnlyCollection<Emploer>),typeof(IReadOnlyCollection<EmploerDTO>));
            return _mapper.Map<IReadOnlyCollection<MoneyTransferSalaryDTO>>(await _unitOfWork.MoneyTransferSalary_Repository.FindByConditionAsync(x => x.Id == id));
        }

        public async Task<IEnumerable<MoneyTransferSalaryDTO>> GetAllAsync()
        {
            //return _mapper.Map((await _unitOfWork.Emploer_Repository.GetAllAsync()), typeof(IReadOnlyCollection<EmploerDTO>), typeof(IReadOnlyCollection<Emploer>));
            return _mapper.Map<IReadOnlyCollection<MoneyTransferSalaryDTO>>(await _unitOfWork.MoneyTransferSalary_Repository.GetAllAsync());
        }
        public async Task<OperationDetail> Update(MoneyTransferSalaryDTO entity)
        {
            var moneyTransferSalary = (await _unitOfWork.MoneyTransferSalary_Repository.FindByConditionAsync(x => x.Id == entity.Id)).FirstOrDefault();
            moneyTransferSalary = (MoneyTransferSalary)_mapper.Map(entity, moneyTransferSalary, typeof(MoneyTransferSalaryDTO), typeof(MoneyTransferSalary));
            await _unitOfWork.SaveChangesAsync();
            return new OperationDetail() { Message = "Save complete" };


        }
    }
}
