﻿using AutoMapper;
using Business_Logic.DTO;
using Data_Access.Infrastructure;
using Data_Access.Models;
using Data_Access.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic.Service
{
    public class TravelHistoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TravelHistoryService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task CreateAsync(TravelHistoryDTO entity)
        {
            await _unitOfWork.TravelHistory_Repository.CreateAsync((TravelHistory)_mapper.Map(entity, typeof(TravelHistoryDTO), typeof(TravelHistory)));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<TravelHistoryDTO>> FindByConditionAsync(int id)
        {
            // return _mapper.Map(await _unitOfWork.Emploer_Repository.FindByConditionAsync(predicat),typeof(IReadOnlyCollection<Emploer>),typeof(IReadOnlyCollection<EmploerDTO>));
            return _mapper.Map<IReadOnlyCollection<TravelHistoryDTO>>(await _unitOfWork.TravelHistory_Repository.FindByConditionAsync(x => x.Id == id));
        }

        public async Task<IEnumerable<TravelHistoryDTO>> GetAllAsync()
        {
            //return _mapper.Map((await _unitOfWork.Emploer_Repository.GetAllAsync()), typeof(IReadOnlyCollection<EmploerDTO>), typeof(IReadOnlyCollection<Emploer>));
            return _mapper.Map<IReadOnlyCollection<TravelHistoryDTO>>(await _unitOfWork.TravelHistory_Repository.GetAllAsync());
        }
        public async Task<OperationDetail> Update(TravelHistoryDTO entity)
        {
            var travelHistory = (await _unitOfWork.TravelHistory_Repository.FindByConditionAsync(x => x.Id == entity.Id)).FirstOrDefault();
            travelHistory = (TravelHistory)_mapper.Map(entity, travelHistory, typeof(TravelHistoryDTO), typeof(TravelHistory));
            await _unitOfWork.SaveChangesAsync();
            return new OperationDetail() { Message = "Save complete" };


        }
    }
}
