﻿using AutoMapper;
using Business_Logic.DTO;
using Data_Access.Infrastructure;
using Data_Access.Models;
using Data_Access.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic.Service
{
    public class UserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task CreateAsync(UserDTO entity)
        {
            await _unitOfWork.UserRepository.CreateAsync((User)_mapper.Map(entity, typeof(UserDTO), typeof(User)));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<UserDTO>> FindByConditionAsync(int id)
        {
            // return _mapper.Map(await _unitOfWork.Emploer_Repository.FindByConditionAsync(predicat),typeof(IReadOnlyCollection<Emploer>),typeof(IReadOnlyCollection<EmploerDTO>));
            return _mapper.Map<IReadOnlyCollection<UserDTO>>(await _unitOfWork.UserRepository.FindByConditionAsync(x => x.Id == id));
        }

        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            //return _mapper.Map((await _unitOfWork.Emploer_Repository.GetAllAsync()), typeof(IReadOnlyCollection<EmploerDTO>), typeof(IReadOnlyCollection<Emploer>));
            return _mapper.Map<IReadOnlyCollection<UserDTO>>(await _unitOfWork.UserRepository.GetAllAsync());
        }
        public async Task<OperationDetail> Update(UserDTO entity)
        {
            var user = (await _unitOfWork.UserRepository.FindByConditionAsync(x => x.Id == entity.Id)).FirstOrDefault();
            user = (User)_mapper.Map(entity, user, typeof(UserDTO), typeof(User));
            await _unitOfWork.SaveChangesAsync();
            return new OperationDetail() { Message = "Save complete" };


        }
    }
}
