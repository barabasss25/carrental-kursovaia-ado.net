﻿using AutoMapper;
using Business_Logic.DTO;
using Data_Access.Infrastructure;
using Data_Access.Models;
using Data_Access.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic.Service
{
    public class EmploerService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public EmploerService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task CreateAsync(EmploerDTO entity)
        {
            await _unitOfWork.Emploer_Repository.CreateAsync(_mapper.Map<Emploer>(entity));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<EmploerDTO>> FindByConditionAsync(int id)
        {
            // return _mapper.Map(await _unitOfWork.Emploer_Repository.FindByConditionAsync(predicat),typeof(IReadOnlyCollection<Emploer>),typeof(IReadOnlyCollection<EmploerDTO>));
            return _mapper.Map <IReadOnlyCollection<EmploerDTO>> (await _unitOfWork.Emploer_Repository.FindByConditionAsync(x=>x.Id ==id));
        }

        public async Task<IEnumerable<EmploerDTO>> GetAllAsync()
        {
            //return _mapper.Map((await _unitOfWork.Emploer_Repository.GetAllAsync()), typeof(IReadOnlyCollection<EmploerDTO>), typeof(IReadOnlyCollection<Emploer>));
            var r = await _unitOfWork.Emploer_Repository.GetAllAsync();
            return _mapper.Map<IReadOnlyCollection<EmploerDTO>>(await _unitOfWork.Emploer_Repository.GetAllAsync());
        }
        public async Task<OperationDetail> Update(EmploerDTO entity)
        {
            var emploer = (await _unitOfWork.Emploer_Repository.FindByConditionAsync(x => x.Id == entity.Id)).FirstOrDefault();
            emploer = (Emploer)_mapper.Map(entity, emploer, typeof(EmploerDTO), typeof(Emploer));
            await _unitOfWork.SaveChangesAsync();
            return new OperationDetail() { Message = "Save complete" };


        }
    }
}
