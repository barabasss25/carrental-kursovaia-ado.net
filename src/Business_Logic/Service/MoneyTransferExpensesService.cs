﻿using AutoMapper;
using Business_Logic.DTO;
using Data_Access.Infrastructure;
using Data_Access.Models;
using Data_Access.Repository;
using Data_Access.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic.Service
{
    public class MoneyTransferExpensesService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public MoneyTransferExpensesService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task CreateAsync(MoneyTransferExpensesDTO entity)
        {
            await _unitOfWork.MoneyTransferExpenses_Repository.CreateAsync((MoneyTransferExpenses)_mapper.Map(entity, typeof(MoneyTransferExpensesDTO), typeof(MoneyTransferExpenses)));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<MoneyTransferExpensesDTO>> FindByConditionAsync(int id)
        {
            // return _mapper.Map(await _unitOfWork.Emploer_Repository.FindByConditionAsync(predicat),typeof(IReadOnlyCollection<Emploer>),typeof(IReadOnlyCollection<EmploerDTO>));
            return _mapper.Map<IReadOnlyCollection<MoneyTransferExpensesDTO>>(await _unitOfWork.MoneyTransferExpenses_Repository.FindByConditionAsync(x => x.Id == id));
        }

        public async Task<IEnumerable<MoneyTransferExpensesDTO>> GetAllAsync()
        {
            //return _mapper.Map((await _unitOfWork.Emploer_Repository.GetAllAsync()), typeof(IReadOnlyCollection<EmploerDTO>), typeof(IReadOnlyCollection<Emploer>));
            return _mapper.Map<IReadOnlyCollection<MoneyTransferExpensesDTO>>(await _unitOfWork.MoneyTransferExpenses_Repository.GetAllAsync());
        }
        public async Task<OperationDetail> Update(MoneyTransferExpensesDTO entity)
        {
            var moneyTransferExpenses = (await _unitOfWork.MoneyTransferExpenses_Repository.FindByConditionAsync(x => x.Id == entity.Id)).FirstOrDefault();
            moneyTransferExpenses = (MoneyTransferExpenses)_mapper.Map(entity, moneyTransferExpenses, typeof(MoneyTransferExpensesDTO), typeof(MoneyTransferExpenses));
            await _unitOfWork.SaveChangesAsync();
            return new OperationDetail() { Message = "Save complete" };


        }
    }
}
