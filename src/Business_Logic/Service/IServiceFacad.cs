﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.Service
{
    public interface IServiceFacad
    {
        TransportService TransportService { get; }
        UserService UserService { get; }
        TravelHistoryService TravelHistoryService { get; }
        EmploerService EmploerService { get; }
        BatteryService BatteryService { get; }
        MoneyTransferExpensesService MoneyTransferExpensesService { get; }
        MoneyTransferSalaryService MoneyTransferSalaryService { get; }
    }
}
