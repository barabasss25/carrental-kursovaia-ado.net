﻿using AutoMapper;
using Business_Logic.DTO;
using Data_Access.Infrastructure;
using Data_Access.Models;
using Data_Access.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic.Service
{
    public class FineService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public FineService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task CreateAsync(FineDTO entity)
        {
            await _unitOfWork.Fine_Repository.CreateAsync((Fine)_mapper.Map(entity, typeof(FineDTO), typeof(Fine)));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<FineDTO>> FindByConditionAsync(int id)
        {
            // return _mapper.Map(await _unitOfWork.Emploer_Repository.FindByConditionAsync(predicat),typeof(IReadOnlyCollection<Emploer>),typeof(IReadOnlyCollection<EmploerDTO>));
            return _mapper.Map<IReadOnlyCollection<FineDTO>>(await _unitOfWork.Fine_Repository.FindByConditionAsync(x => x.Id == id));
        }

        public async Task<IReadOnlyCollection<FineDTO>> GetAllAsync()
        {
            //return _mapper.Map((await _unitOfWork.Emploer_Repository.GetAllAsync()), typeof(IReadOnlyCollection<EmploerDTO>), typeof(IReadOnlyCollection<Emploer>));
            return _mapper.Map<IReadOnlyCollection<FineDTO>>(await _unitOfWork.Fine_Repository.GetAllAsync());
        }
        public async Task<OperationDetail> Update(FineDTO entity)
        {
            var fine = (await _unitOfWork.Fine_Repository.FindByConditionAsync(x => x.Id == entity.Id)).FirstOrDefault();
            fine = (Fine)_mapper.Map(entity, fine, typeof(FineDTO), typeof(Fine));
            await _unitOfWork.SaveChangesAsync();
            return new OperationDetail() { Message = "Save complete" };


        }
    }
}
