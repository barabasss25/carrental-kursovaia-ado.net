﻿using AutoMapper;
using Business_Logic.DTO;
using Data_Access.Infrastructure;
using Data_Access.Models;
using Data_Access.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic.Service
{
    public class BatteryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public BatteryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task CreateAsync(IUnitOfWork unitOfWork,BatteryDTO entity)
        {
            await _unitOfWork.Battery_Repository.CreateAsync((Battery)_mapper.Map(entity, typeof(BatteryDTO), typeof(Battery)));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<BatteryDTO>> FindByConditionAsync(int id)
        {
            // return _mapper.Map(await _unitOfWork.Emploer_Repository.FindByConditionAsync(predicat),typeof(IReadOnlyCollection<Emploer>),typeof(IReadOnlyCollection<EmploerDTO>));
            return _mapper.Map<IReadOnlyCollection<BatteryDTO>>(await _unitOfWork.Emploer_Repository.FindByConditionAsync(x => x.Id == id));
        }

        public async Task<IEnumerable<EmploerDTO>> GetAllAsync()
        {
            //return _mapper.Map((await _unitOfWork.Emploer_Repository.GetAllAsync()), typeof(IReadOnlyCollection<EmploerDTO>), typeof(IReadOnlyCollection<Emploer>));
            return _mapper.Map<IReadOnlyCollection<EmploerDTO>>(await _unitOfWork.Emploer_Repository.GetAllAsync());
        }
        public async Task<OperationDetail> Update(BatteryDTO entity)
        {
            var battery = (await _unitOfWork.Battery_Repository.FindByConditionAsync(x => x.Id == entity.Id)).FirstOrDefault();
            battery = (Battery)_mapper.Map(entity, battery, typeof(BatteryDTO), typeof(Battery));
            await _unitOfWork.SaveChangesAsync();
            return new OperationDetail() { Message = "Save complete" };


        }
    }
}
