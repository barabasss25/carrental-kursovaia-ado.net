﻿using AutoMapper;
using Business_Logic.DTO;
using Data_Access.Infrastructure;
using Data_Access.Models;
using Data_Access.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic.Service
{
    public class TransportService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public TransportService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task CreateAsync(TransportDTO entity)
        {
            await _unitOfWork.Transport_Repository.CreateAsync((Transport)_mapper.Map(entity, typeof(TransportDTO), typeof(Transport)));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<TransportDTO>> FindByConditionAsync(int id)
        {
            // return _mapper.Map(await _unitOfWork.Emploer_Repository.FindByConditionAsync(predicat),typeof(IReadOnlyCollection<Emploer>),typeof(IReadOnlyCollection<EmploerDTO>));
            return _mapper.Map<IReadOnlyCollection<TransportDTO>>(await _unitOfWork.Transport_Repository.FindByConditionAsync(x => x.Id == id));
        }

        public async Task<IEnumerable<TransportDTO>> GetAllAsync()
        {
            //return _mapper.Map((await _unitOfWork.Emploer_Repository.GetAllAsync()), typeof(IReadOnlyCollection<EmploerDTO>), typeof(IReadOnlyCollection<Emploer>));
            return _mapper.Map<IReadOnlyCollection<TransportDTO>>(await _unitOfWork.Transport_Repository.GetAllAsync());
        }
        public async Task<OperationDetail> Update(TransportDTO entity)
        {
            var transport = (await _unitOfWork.Transport_Repository.FindByConditionAsync(x => x.Id == entity.Id)).FirstOrDefault();
            transport = (Transport)_mapper.Map(entity, transport, typeof(TransportDTO), typeof(Transport));
            await _unitOfWork.SaveChangesAsync();
            return new OperationDetail() { Message = "Save complete" };


        }
    }
}
