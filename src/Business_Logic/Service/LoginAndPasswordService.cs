﻿using AutoMapper;
using Business_Logic.DTO;
using Data_Access.Infrastructure;
using Data_Access.Models;
using Data_Access.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic.Service
{
    public class LoginAndPasswordService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public LoginAndPasswordService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task CreateAsync(LoginAndPasswordDTO entity)
        {
            await _unitOfWork.LoginAndPassword_Repository.CreateAsync((LoginAndPassword)_mapper.Map(entity, typeof(LoginAndPasswordDTO), typeof(LoginAndPassword)));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<LoginAndPasswordDTO>> FindByConditionAsync(int id)
        {
            // return _mapper.Map(await _unitOfWork.Emploer_Repository.FindByConditionAsync(predicat),typeof(IReadOnlyCollection<Emploer>),typeof(IReadOnlyCollection<EmploerDTO>));
            return _mapper.Map<IReadOnlyCollection<LoginAndPasswordDTO>>(await _unitOfWork.LoginAndPassword_Repository.FindByConditionAsync(x => x.Id == id));
        }

        public async Task<IEnumerable<LoginAndPasswordDTO>> GetAllAsync()
        {
            //return _mapper.Map((await _unitOfWork.Emploer_Repository.GetAllAsync()), typeof(IReadOnlyCollection<EmploerDTO>), typeof(IReadOnlyCollection<Emploer>));
            return _mapper.Map<IReadOnlyCollection<LoginAndPasswordDTO>>(await _unitOfWork.LoginAndPassword_Repository.GetAllAsync());
        }
        public async Task<OperationDetail> Update(LoginAndPasswordDTO entity)
        {
            var loginAndPassword = (await _unitOfWork.LoginAndPassword_Repository.FindByConditionAsync(x => x.Id == entity.Id)).FirstOrDefault();
            loginAndPassword = (LoginAndPassword)_mapper.Map(entity, loginAndPassword, typeof(LoginAndPasswordDTO), typeof(LoginAndPassword));
            await _unitOfWork.SaveChangesAsync();
            return new OperationDetail() { Message = "Save complete" };


        }
    }
}
