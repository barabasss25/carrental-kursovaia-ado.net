﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.Service
{
    public class EmploerServiceFacad : IServiceFacad
    {
       public EmploerServiceFacad(MoneyTransferExpensesService moneyTransferExpensesService, MoneyTransferSalaryService moneyTransferSalaryService , TransportService transportService, UserService userService, TravelHistoryService travelHistoryService, EmploerService emploerService, BatteryService batteryService)
        {
            MoneyTransferExpensesService = moneyTransferExpensesService;
            MoneyTransferSalaryService = moneyTransferSalaryService;
            TransportService = transportService;
            UserService = userService;
            TravelHistoryService = travelHistoryService;
            EmploerService = emploerService;
            BatteryService = batteryService;
        }
        public TransportService TransportService { get; }

        public UserService UserService { get; }

        public TravelHistoryService TravelHistoryService { get; }

        public EmploerService EmploerService { get; }

        public BatteryService BatteryService { get; }
        public MoneyTransferSalaryService MoneyTransferSalaryService { get; }
        public MoneyTransferExpensesService MoneyTransferExpensesService { get; }


    }
}
