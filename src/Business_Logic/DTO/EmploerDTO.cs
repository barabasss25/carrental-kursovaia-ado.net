﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.DTO
{
    public class EmploerDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Position { get; set; }
        public int LoginAndPasswordId { get; set; }
        public LoginAndPasswordDTO LoginAndPassword { get; set; }
        public bool Online { get; set; }
        public DateTime TimeStartWorked { get; set; } = DateTime.Now;
        public List<MoneyTransferExpensesDTO> moneyTransferExpenses { get; set; } // Для этих классов тоже ДТО и тут заменить на дто 
        public List<MoneyTransferSalaryDTO> moneyTransferSalaries{ get; set; }
    }
}

