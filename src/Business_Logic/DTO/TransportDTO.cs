﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.DTO
{
    public class TransportDTO
    {
        public int Id { get; set; }
        public UserDTO User { get; set; }
        public string TypeTransport { get; set; }
        public float CostPerHouse { get; set; }
        public string GeoLocation { get; set; }
        public int BatteryId { get; set; }
        public BatteryDTO Battery { get; set; }
        public List<TravelHistoryDTO> TravelHistories { get; set; }
        public List<MoneyTransferExpensesDTO> MoneyTransferExpenses { get; set; }
    }
}
