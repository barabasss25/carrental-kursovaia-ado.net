﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.DTO
{
    public class MoneyTransferSalaryDTO
    {
        public int Id { get; set; }
        public EmploerDTO Emploer { get; set; }
     //   public DateTime StartWorkDay { get; set; } = DateTime.Now;
        public int HoursWorked { get; set; }
        public List<FineDTO> Fines { get; set; }
    }
}
