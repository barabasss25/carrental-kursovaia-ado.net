﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public int LoginAndPasswordId { get; set; }
        public LoginAndPasswordDTO LoginAndPassword { get; set; }
        public string Name { get; set; }
        public double Money { get; set; }
        public string Number { get; set; }
        public string Telegram { get; set; }
        public double Bonuses { get; set; }
        public string DateOfBirth { get; set; }
        public int TransportId { get; set; }
        public TransportDTO Transport { get; set; }
        public List<TravelHistoryDTO> TravelHistories { get; set; }
        public List<MoneyTransferExpensesDTO> MoneyTransferExpenses { get; set; }
    }
}
