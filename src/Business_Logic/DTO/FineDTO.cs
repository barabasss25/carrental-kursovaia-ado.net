﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.DTO
{
    public class FineDTO
    {
        public int Id { get; set; }
        public EmploerDTO Emploer { get; set; }
        public string Description { get; set; }
        public float Amount { get; set; }
        public MoneyTransferSalaryDTO MoneyTransferSalary { get; set; }
    }
}
