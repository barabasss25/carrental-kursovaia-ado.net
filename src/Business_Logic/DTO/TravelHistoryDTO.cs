﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.DTO
{
    public class TravelHistoryDTO
    {
        public int Id { get; set; }
        public UserDTO User { get; set; }
        public TransportDTO Transport { get; set; }
        public float Cost { get; set; }
        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; } = DateTime.Now;
    }
}
