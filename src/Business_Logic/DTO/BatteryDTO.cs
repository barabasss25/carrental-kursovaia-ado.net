﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.DTO
{
    public class BatteryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Recharge { get; set; }
        public bool IfBroken { get; set; }
        public TransportDTO Transport { get; set; }
    }
}
