﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.DTO
{
    public class MoneyTransferExpensesDTO
    {
        public int Id { get; set; }
        public UserDTO User { get; set; }
        public EmploerDTO Emploer { get; set; }
        public string Description { get; set; }
        public double Money { get; set; }
        public TransportDTO Transport { get; set; }
    }
}
