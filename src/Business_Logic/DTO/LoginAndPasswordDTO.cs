﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.DTO
{
    public class LoginAndPasswordDTO
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public DateTime DateRegistration { get; set; } = DateTime.Now;
        public UserDTO User { get; set; }
        public EmploerDTO Emploer { get; set; }
    }
}
