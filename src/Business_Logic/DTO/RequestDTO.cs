﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.DTO
{
    public class RequestDTO
    {
        public int Id { get; set; }
        public EmploerDTO EmploerSetRequest { get; set; }
        public string Description { get; set; }
    }
}
