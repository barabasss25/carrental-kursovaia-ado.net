﻿using AutoMapper;
using Business_Logic.DTO;
using Data_Access.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.Infrastructure.AutoMaper
{
   public class MapperConfig:Profile
    {
        public MapperConfig()
        {
            CreateMap<EmploerDTO, Emploer>();
            CreateMap<Battery, BatteryDTO>();
            CreateMap<Emploer, EmploerDTO>();
            CreateMap<Fine, FineDTO>();
            CreateMap<LoginAndPasswordDTO, LoginAndPassword>();
            CreateMap<LoginAndPassword, LoginAndPasswordDTO>();
            CreateMap<MoneyTransferExpenses, MoneyTransferExpensesDTO>();
            CreateMap<MoneyTransferSalary, MoneyTransferSalaryDTO>();
            CreateMap<Request, RequestDTO>();
            CreateMap<Transport, TransportDTO>();
            CreateMap<TravelHistory, TravelHistoryDTO>();
            CreateMap<User, UserDTO>();
            CreateMap<IReadOnlyCollection<Emploer>, IReadOnlyCollection<EmploerDTO>>();
            CreateMap<IReadOnlyCollection<EmploerDTO>, IReadOnlyCollection<Emploer>>();
        }
    }
}
