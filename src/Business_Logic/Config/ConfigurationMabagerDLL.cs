﻿using Business_Logic.Service;
using Data_Access.Context;
using Data_Access.Repository;
using Data_Access.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business_Logic.Config
{
    public class ConfigurationManagerBLL
    {
        public static void Configuration(IServiceCollection service, string connection)
        {
            service.AddDbContext<ContextDb>(option => option.UseSqlServer(connection));
            service.AddTransient(typeof(UserRepository));
            service.AddTransient(typeof(EmploerService));
            service.AddTransient(typeof(TransportRepository));
            service.AddTransient(typeof(EmploerRepository));
            service.AddTransient(typeof(BatteryRepository));
            service.AddTransient(typeof(LoginAndPasswordRepository));
            service.AddTransient(typeof(MoneyTransferExpensesRepository));
            service.AddTransient(typeof(MoneyTransferSalaryRepository));
            service.AddTransient(typeof(RequestRepository));
            service.AddTransient(typeof(TravelHistoryRepository));
            service.AddTransient(typeof(FineRepository));
            service.AddSingleton(typeof(IUnitOfWork), typeof(UnitOfWork));
        }


    }
}
